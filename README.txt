Views Alphabetic Pagination Navigator
-------------------------------------

The Alpha Numeric Glossary for Views module enables you to add an alphabetical & numeric way of pagination in the header or footer of a views display.


Views Integration
-----------------
1) Create a new view of either users, content (nodes), taxonomy or comments.
2) Add fields in the contextual filters (eg. title). Make the Provide default value and set the Fixed value as any name (eg. a).
3) Click on the More tab to make the Glossary mode.
4) Add either a header or a footer to your view. Select the new item available in the menu of options for Global: Alpha Numeric Glossary.
5) There is a setting where you can able to display all the alphabetic including numeric (optional)
6) You can also set the count for each alphabetic or numeric.
7) Define the Text & Link as Upper or Lower case.
